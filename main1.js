
const createReceta=()=>{

    const urlCook='http://localhost:3000/recetas';
    
  fetch(urlCook).then(res => res.json()).then(recetas=> {

    console.log(recetas);

   for (receta of recetas){
      
        const nombre$$ = document.createElement ('h3');
        //const diventrantes$$ = document.querySelector('.recetas__entrante');
       // const divprincipales$$ = document.querySelector('.recetas__principal');
        const sectionprincipales$$ = document.querySelector('.recetas__principales-section');
        const divpostres$$ = document.querySelector('.recetas__postre');
        nombre$$.innerHTML = receta.name;
        const tipo$$ = document.createElement('p');
        tipo$$.innerHTML = receta.tipo;  

        const btndiv$$ = document.createElement('button');
        btndiv$$.textContent = 'Ver mas';
        btndiv$$.classList.add('btn_filter');
        btndiv$$.classList.add('btn');

        
        if(tipo$$.textContent == 'Entrante'){
          const div$$ = document.createElement ('div');
          div$$.classList.add('recetas__card');
          div$$.appendChild(nombre$$);
          diventrantes$$.appendChild(div$$);
          //console.log(nombre$$);

          const divimg$$ = document.createElement ('div');
          divimg$$.classList.add('recetas__description');
          const img$$ = document.createElement('img');
          img$$.src = receta.photo;
          
          img$$.classList.add('recetas__img');
          divimg$$.appendChild(img$$);
          div$$.appendChild(divimg$$);
          
          const divingredi$$ = document.createElement('div');
          divingredi$$.classList.add('recetas__ingredients');

          const btnInfo$$ = document.createElement('button');
          btnInfo$$.classList.add('btn_filter');
          btnInfo$$.classList.add('btn');
          btnInfo$$.textContent= 'Mas informacion';
          div$$.appendChild(btnInfo$$);
          

    


          for(let i=0; i<receta.ingredients.length;i++){
            const ingredients$$ = document.createElement('p');
            ingredients$$.innerHTML =  receta.ingredients[i];
            
             var rece = receta.ingredients;
                
                  divingredi$$.insertAdjacentElement("beforeend", ingredients$$);
                  divimg$$.appendChild(divingredi$$);

          }

          div$$.appendChild(btndiv$$);
          btndiv$$.addEventListener('click', expandir);

          
          function expandir(){
            divingredi$$.style.height = 'auto';
            btndiv$$.addEventListener('click', contraer = () =>{
              divingredi$$.style.height = '12rem';
              
            });
          }
        

          }
           else if(tipo$$.textContent == 'Principal'){
            const div2$$ = document.createElement ('div');
            div2$$.classList.add('recetas__thumb');
            //console.log(nombre$$);
            div2$$.appendChild(nombre$$);
           // divprincipales$$.appendChild(div2$$);
           sectionprincipales$$.appendChild(div2$$);

           const divimg2$$ = document.createElement('img');
           divimg2$$.classList.add('recetas__img');
           divimg2$$.src = receta.photo;
           sectionprincipales$$.appendChild(divimg2$$);

           const p$$ = document.createElement ('p');
           p$$.innerHTML = receta.preparation;
           div2$$.insertAdjacentElement("afterend", p$$)

           //div2$$.appendChild(p$$);


           
      
          }
          else if(tipo$$.textContent == 'Postre'){
            const div3$$ = document.createElement ('div');
            div3$$.classList.add('recetas__thumb');
            //console.log(nombre$$);
            div3$$.appendChild(nombre$$);
            divpostres$$.appendChild(div3$$);
          }

        

    }

    }
    
  )};

window.onload=() =>{
    createReceta();
}

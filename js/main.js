window.onload=() =>{
    
    Inicio();
    welcome();

    
}
const divPrincipal$$ = document.querySelector('div');
divPrincipal$$.classList.add('divPrincipal');

//-------------HEADER-------------//
const header = () => {

const header$$ = document.createElement('header');
header$$.classList.add('container', 'container__photo');
const tittle$$ = document.createElement('h1');
tittle$$.classList.add('container__title');
tittle$$.textContent=('Qcomemos!?');
const divSubtitle$$ = document.createElement('div');
divSubtitle$$.classList.add('container__subtitle');
const subttitle$$ = document.createElement('h2');
subttitle$$.textContent=('Recetas convencionales y diferentes');
const pSubtitle$$ = document.createElement('p');
pSubtitle$$.textContent=('Encuentra sabrosas recetas');
const nav$$ = document.createElement('nav');
nav$$.classList.add('navbar', 'navbar-expand-md');
nav$$.setAttribute('role', 'navigation');
const divNav$$ = document.createElement('div');
divNav$$.classList.add('container-fluid');
const btnNav$$ = document.createElement('button');
btnNav$$.classList.add('navbar-toggler','mb-2','container__btn'); // para que aparezca cuandos sea mvl
btnNav$$.setAttribute('role', 'button');
btnNav$$.setAttribute('data-bs-toggle', 'collapse');
btnNav$$.setAttribute('data-bs-target', '#navbarToggle');
btnNav$$.setAttribute('aria-controls', "navbarToggler");
btnNav$$.setAttribute('aria-expand', "false");

const iconBtnNav$$ = document.createElement('img');
iconBtnNav$$.src ="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAAkCAYAAADhAJiYAAAAvElEQVRYR+2Y4QmDMBBGn5vU0TpJ6yaOUDfpKO0ElUCUooXvfiT1DOcfkYTj5eWLkOtw9nTOeDgF0B24ZXMDkL4/hU0+gFT7ua37y9A/gBLHC+jze+U6EihBXIHx29LRQEsk3BhqF2i3MuMp3J7WYoYCqPaPcdnhYltmjIyc1i5QhFqF2p0hmVbjhHZDbRQgp4UhpSgMhSFlQI1HhsKQMqDGXV0U38DFy1V6yk0MU7NBaa46for+UFUDqvgMHz2IJeJF0cwAAAAASUVORK5CYII=";

    //----------Menu desplegable

const divDesplegable$$ = document.createElement('div');
divDesplegable$$.classList.add('collapse','navbar-collapse', 'navbar__color');
divDesplegable$$.setAttribute('id', 'navbarToggle');

const ulNav$$ = document.createElement('ul');
ulNav$$.classList.add('navbar-nav', 'me-auto', 'mb-2', 'mb-lg-0');

const liNav$$ = document.createElement('li');
liNav$$.classList.add('nav-item', 'mx-4');
const aNav$$ = document.createElement('a');
aNav$$.textContent=('Inicio');
aNav$$.addEventListener('click', VolverInicio);
aNav$$.classList.add('nav-link');
aNav$$.setAttribute('href', '#');

const liNav2$$ = document.createElement('li');
liNav2$$.classList.add('nav-item', 'mx-4');
const aNav2$$ = document.createElement('a');
aNav2$$.textContent=('Recetas');
aNav2$$.classList.add('nav-link');
aNav2$$.setAttribute('href', '#');

const liNav3$$ = document.createElement('li');
liNav3$$.classList.add('nav-item', 'mx-4');
const aNav3$$ = document.createElement('a');
aNav3$$.textContent=('Inicia sesion');
aNav3$$.classList.add('nav-link');
aNav3$$.setAttribute('href', '#');
aNav3$$.addEventListener('click', welcome)

//Search del menu

const formNav$$ = document.createElement('form');
formNav$$.classList.add('d-flex');
const inputFormNav$$ = document.createElement('input');
inputFormNav$$.classList.add('form-control', 'm-2','inputNav');
inputFormNav$$.setAttribute('type', 'search');
inputFormNav$$.setAttribute('placeholder', 'Buscar...');

const btnFilter$$ = document.createElement('button');
btnFilter$$.textContent=('Filtrar');
btnFilter$$.classList.add('btn', 'btn-outline-success','btn_filter','m-2');


//------------AppendChild HEADER----------//
divPrincipal$$.insertAdjacentElement("afterbegin",header$$);
header$$.appendChild(tittle$$);
header$$.appendChild(divSubtitle$$);
divSubtitle$$.appendChild(subttitle$$);
divSubtitle$$.appendChild(pSubtitle$$);
header$$.appendChild(nav$$);
nav$$.appendChild(divNav$$);
divNav$$.appendChild(btnNav$$);
btnNav$$.appendChild(iconBtnNav$$);

//Lista desplegable
divNav$$.appendChild(divDesplegable$$);
divDesplegable$$.appendChild(ulNav$$);
ulNav$$.appendChild(liNav$$);
liNav$$.appendChild(aNav$$);
ulNav$$.appendChild(liNav2$$);
liNav2$$.appendChild(aNav2$$);
ulNav$$.appendChild(liNav3$$);
liNav3$$.appendChild(aNav3$$);

divDesplegable$$.appendChild(formNav$$);
formNav$$.appendChild(inputFormNav$$);
formNav$$.appendChild(btnFilter$$);
}
//----------------BODY----------------------//
const carousel = () => {
const sectionEntrada$$ = document.createElement('section');
sectionEntrada$$.classList.add('container', 'recetas');
const spantitle$$ = document.createElement('span');
spantitle$$.textContent=('Encuentra tu receta ideal');
spantitle$$.classList.add('recetas__title');


//--------------CARROUSEL----------//
const diventrada$$ = document.createElement('div');
diventrada$$.classList.add('carousel', 'slide', 'mt-2', 'carousel__style');
diventrada$$.setAttribute('data-bs-ride', 'carousel');
diventrada$$.setAttribute('id', 'carouselDiv');

const divCarousel$$ = document.createElement('div');
divCarousel$$.classList.add('carousel-inner');

const divItemCarousel$$ = document.createElement('div');
divItemCarousel$$.classList.add('carousel-item', 'active', 'mb-3');
divItemCarousel$$.setAttribute('data-bs-interval', '5000');

const imgItem$$ = document.createElement('img');
imgItem$$.classList.add('imgItem');
imgItem$$.src = '/assets/carrousel1.jpg';
const spanImg$$ = document.createElement('span');
spanImg$$.textContent=('Potencia tus comidas');
spanImg$$.classList.add('carousel__txt','carousel__txt-especias');

const divItemCarousel2$$ = document.createElement('div');
divItemCarousel2$$.classList.add('carousel-item', 'mb-3');
divItemCarousel2$$.setAttribute('data-bs-interval', '5000');

const imgItem2$$ = document.createElement('img');
imgItem2$$.classList.add('imgItem');
imgItem2$$.src = '/assets/carrousel2.jpg';
const spanImg2$$ = document.createElement('span');
spanImg2$$.textContent=('Cuerpo sano, mente sana');
spanImg2$$.classList.add('carousel__txt','carousel__txt-sano');

const divItemCarousel3$$ = document.createElement('div');
divItemCarousel3$$.classList.add('carousel-item', 'mb-3');
divItemCarousel3$$.setAttribute('data-bs-interval', '5000');

const imgItem3$$ = document.createElement('img');
imgItem3$$.classList.add('imgItem');
imgItem3$$.src = '/assets/carrousel3.jpg';
const spanImg3$$ = document.createElement('span');
spanImg3$$.textContent=('Aprende a preparar postres');
spanImg3$$.classList.add('carousel__txt','carousel__txt-postres');

const aFlecha$$ = document.createElement('a');
aFlecha$$.classList.add('carousel-control-prev');
aFlecha$$.setAttribute('href', '#carouselDiv');
aFlecha$$.setAttribute('data-bs-slide', 'prev');
const spanIcon$$ = document.createElement('span');
spanIcon$$.classList.add('carousel-control-prev-icon');


const aFlecha2$$ = document.createElement('a');
aFlecha2$$.classList.add('carousel-control-next');
aFlecha2$$.setAttribute('href', '#carouselDiv');
aFlecha2$$.setAttribute('data-bs-slide', 'next');
const spanIcon2$$ = document.createElement('span');
spanIcon2$$.classList.add('carousel-control-next-icon');
//----------APPENDCHILD CAROUSEL------------//
sectionEntrada$$.appendChild(spantitle$$);
divPrincipal$$.appendChild(sectionEntrada$$)
//main.appendChild(sectionEntrada$$);
sectionEntrada$$.appendChild(diventrada$$);
diventrada$$.appendChild(divCarousel$$);
divCarousel$$.appendChild(divItemCarousel$$);

divItemCarousel$$.appendChild(spanImg$$);
divItemCarousel$$.appendChild(imgItem$$);

divItemCarousel2$$.appendChild(spanImg2$$);

divCarousel$$.appendChild(divItemCarousel2$$);
divItemCarousel2$$.appendChild(imgItem2$$);
divCarousel$$.appendChild(divItemCarousel3$$);
divItemCarousel3$$.appendChild(spanImg3$$);
divItemCarousel3$$.appendChild(imgItem3$$);
diventrada$$.appendChild(aFlecha$$);
aFlecha$$.appendChild(spanIcon$$);
diventrada$$.appendChild(aFlecha2$$);
aFlecha2$$.appendChild(spanIcon2$$);

//---------FIN CAROUSEL------//
}
//---------POST ESPECIALES-------//
const specialPost = () => {
const sectionSpecialPost = document.createElement('section');
sectionSpecialPost.classList.add('mt-3', 'specialpost', 'mx-3');
const divSpecialPost = document.createElement('div');
divSpecialPost.classList.add('container','specialPost__card');
const divSpecialTxt = document.createElement('div');
divSpecialTxt.classList.add('specialPost__card-txt');
const tittleSpecialPost = document.createElement('h3');
tittleSpecialPost.textContent = ('Próxima receta');
const txtSpecialPost = document.createElement('p');
txtSpecialPost.classList.add('specialPost__txt');
txtSpecialPost.textContent=('La semana que viene aprenderemos a cocinar...')
const txtSpecialPost2= document.createElement('span');
txtSpecialPost2.textContent = ('Pizza casera');
txtSpecialPost2.classList.add('specialPost__receta-title')
const divImgPostEspecial = document.createElement('div');
divImgPostEspecial.classList.add('specialpost__card-img');
const ImgPostEspecial = document.createElement('img');
ImgPostEspecial.src = '/assets/pizza.jpg';
ImgPostEspecial.classList.add('specialpost__img');


const divSpecialPost2 = document.createElement('div');
divSpecialPost2.classList.add('container', 'specialPost__card');
const divSpecialTxtC2 = document.createElement('div');
divSpecialTxtC2.classList.add('specialPost__card-txt');
const tittleSpecialPostC2 = document.createElement('h3');
tittleSpecialPostC2.textContent = ('TOP Nº1 de la semana');
const txtSpecialPostC2 = document.createElement('p');
txtSpecialPostC2.classList.add('specialPost__txt');
txtSpecialPostC2.textContent=('La receta mas visitada ha sido...')
const spanSpecialPost2= document.createElement('span');
spanSpecialPost2.textContent = ('Pulpo a la gallega');
spanSpecialPost2.classList.add('specialPost__receta-title')
const divImgPostEspecialC2 = document.createElement('div');
divImgPostEspecialC2.classList.add('specialpost__card-img');
const ImgPostEspecialC2 = document.createElement('img');
ImgPostEspecialC2.src = '/assets/pulpo.jpg';
ImgPostEspecialC2.classList.add('specialpost__img');

divPrincipal$$.appendChild(sectionSpecialPost);
//main.appendChild(sectionSpecialPost);
sectionSpecialPost.appendChild(divSpecialPost);
divSpecialPost.appendChild(divSpecialTxt);
divSpecialTxt.appendChild(tittleSpecialPost);
divSpecialTxt.appendChild(txtSpecialPost);
divSpecialTxt.appendChild(txtSpecialPost2);
divSpecialPost.appendChild(divImgPostEspecial);
divImgPostEspecial.appendChild(ImgPostEspecial);

sectionSpecialPost.appendChild(divSpecialPost2);
divSpecialPost2.appendChild(divSpecialTxtC2);
divSpecialTxtC2.appendChild(tittleSpecialPostC2);
divSpecialTxtC2.appendChild(txtSpecialPostC2);
divSpecialTxtC2.appendChild(spanSpecialPost2);
divSpecialPost2.appendChild(divImgPostEspecialC2);
divImgPostEspecialC2.appendChild(ImgPostEspecialC2);

//--------FIN POST ESPECIALES-----------//
}
//--------SECTION MOSTRAR TIPOS DE RECETAS-------//

  











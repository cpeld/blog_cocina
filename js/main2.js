const Inicio = () =>{
  header();
  carousel();
  specialPost();
  createReceta();
  //recetario();
}
const VolverInicio = () =>{
  divPrincipal$$.innerHTML='';
  Inicio();
}
const welcome = () =>{
    const divRegistro = document.createElement('div');
    divRegistro.classList.add('registro__container', 'container');
    const form = document.createElement('form');
    form.classList.add('form-signin', 'registro__formulario');
    const imgLogin = document.createElement('img');
    imgLogin.classList.add('registro__imgLogin');
    imgLogin.src=('assets/login.jpeg');
    const acerrar = document.createElement('a');
    acerrar.addEventListener('click', VolverInicio);
    const imgClose = document.createElement('img');
    imgClose.classList.add('registro__botonCerrar');
    imgClose.src=('/assets/cerrar.png');
    const titleLogin = document.createElement('h1');
    titleLogin.classList.add('registro__titleLogin');
    titleLogin.textContent=('Bienvenido a Qcomemos');
    const subtitleLogin = document.createElement('p');
    subtitleLogin.textContent=('Inicia sesion');
    subtitleLogin.classList.add('my-3')
    const labelLoginEmail = document.createElement('label');
    //labelLoginEmail.textContent=('Email Address');
    const inputLoginEmail = document.createElement('input');
    //inputLoginEmail.classList.add('form-control');
    inputLoginEmail.setAttribute('type', 'email');
    inputLoginEmail.setAttribute('placeholder', 'Email')
    const inputLoginPassword = document.createElement('input');
    inputLoginPassword.setAttribute('type', 'password');
    inputLoginPassword.setAttribute('placeholder', 'Password');
    const divLoginCheck = document.createElement('div');
    
    const labelCheck = document.createElement('label');
    labelCheck.textContent = ('Remember me');
    labelCheck.classList.add('my-4')
    const inputCheck = document.createElement('input');
    inputCheck.classList.add('mx-2')
    inputCheck.setAttribute('type', 'checkbox');
    inputCheck.setAttribute('value', 'Remember');

    const btnLogin = document.createElement('button');
    btnLogin.classList.add('registro__boton', 'mb-4');
    btnLogin.setAttribute('type', 'submit');
    btnLogin.textContent=('Sign in');

    const txtRegistro = document.createElement('p');
    txtRegistro.textContent=('Crea una cuenta nueva pinchando aqui');
    txtRegistro.classList.add('registro__txt__a');
    const aRegistro = document.createElement('a');
    aRegistro.addEventListener('click', registro);

    divPrincipal$$.appendChild(divRegistro);
    divRegistro.appendChild(form);
    form.appendChild(imgLogin);
    form.appendChild(acerrar)
    acerrar.appendChild(imgClose)
    form.appendChild(titleLogin);
    form.appendChild(subtitleLogin)
    form.appendChild(labelLoginEmail)
    form.appendChild(inputLoginEmail)
    form.appendChild(inputLoginPassword)
    form.appendChild(divLoginCheck);
    divLoginCheck.appendChild(labelCheck);
    labelCheck.appendChild(inputCheck)
    form.appendChild(btnLogin);
    form.appendChild(aRegistro)
    aRegistro.appendChild(txtRegistro)


}
const registro = () =>{
  divPrincipal$$.innerHTML='';
  const formRegistro = document.createElement('form');
  formRegistro.classList.add('needs-validation');
  const divForm = document.createElement('div');
  divForm.classList.add('row', 'registro__formulario', 'registro__newUser');


  const labelName = document.createElement('label');
  labelName.textContent=('Nombre');
  labelName.classList.add('mt-4', 'mb-1');
  const inputName = document.createElement('input');
  inputName.classList.add('col-md-4', 'mb-3');
  inputName.setAttribute('placeholder', 'Name');
  const inputSurname = document.createElement('input');
  inputSurname.classList.add('col-md-4','mb-3', 'mx-2');
  inputSurname.setAttribute('placeholder', 'Last Name');

  const labelUser = document.createElement('label');
  labelUser.classList.add('mt-3')
  labelUser.textContent=('Username')
  const divgroup = document.createElement('div');
  divgroup.classList.add('input-group', 'mt-1');
  const divgroup2 = document.createElement('div');
  divgroup2.classList.add('input-group-prepend');
  const userName = document.createElement ('span');
  userName.classList.add('input-group-text');
  userName.textContent=('@');
  const inputUser = document.createElement('input');
  inputUser.classList.add('w-50')
  inputUser.setAttribute('placeholder', 'UserName')
  inputUser.setAttribute('type', 'text');
  inputUser.setAttribute('required', 'true');

  const labelEmail= document.createElement('label');
  labelEmail.classList.add('email', 'mt-3','mb-1');
  labelEmail.textContent=('Email')
  const inputEmail = document.createElement('input');
  inputEmail.setAttribute('placeholder', 'Email');
  const btnRegistro = document.createElement('button');
  btnRegistro.classList.add('my-3', 'registro__boton')
  btnRegistro.textContent=('Registrarse');
  btnRegistro.setAttribute('type', 'submit');

  divPrincipal$$.appendChild(formRegistro);
  formRegistro.appendChild(divForm);
  divForm.appendChild(labelName)
  divForm.appendChild(inputName)
  divForm.appendChild(inputSurname)
  divForm.appendChild(labelUser);
  labelUser.appendChild(divgroup)
  divgroup.appendChild(divgroup2);
  divgroup2.appendChild(userName);
  divgroup.appendChild(inputUser);
  divForm.appendChild(labelEmail)
  divForm.appendChild(inputEmail);
  divForm.appendChild(btnRegistro);
}
const createReceta = () => {

    const urlCook='http://localhost:3000/recetas';
 
  fetch(urlCook).then(res => res.json()).then(recetas=> {
    console.log(recetas);

    const divSelector= document.querySelector('#recetas__entrantes');
    const divPlato =document.querySelector('#recetas__principales')
    const botonFilter = document.querySelector('.btn_filter')
    const inputFilter = document.querySelector('.inputNav');


    //-------------Section Recetario-----------//

      const sectionRecetario$$ = document.createElement('section');
      sectionRecetario$$.classList.add('recetas', 'mt-3', 'container', 'recetas__grupos__section', 'py-1');
      const divRecetario = document.createElement('div');
      divRecetario.classList.add('recetas__grupos');
      const divRecEntrante = document.createElement('div');
      divRecEntrante.classList.add('recetas__grupos__entrante', 'recetas_tipo');
      
      const btnRecEntre = document.createElement('a');
      btnRecEntre.addEventListener('click', ()=>{
          verEntrantes();
          
      });
      btnRecEntre.classList.add('recetas__grupos__boton','btn__rec');
  
      const titleRec = document.createElement('h3');
      titleRec.textContent = 'Entrantes';
      const p = document.createElement('p');
      p.classList.add('recetas__grupos__txt');
      p.textContent='Pulsa aqui para ver todas las recetas de entrantes';
      const divimg = document.createElement('div');
      divimg.classList.add('recetas__grupos__divimg');
      const imgbtn = document.createElement('img');
      imgbtn.classList.add('recetas__grupos__img')
      imgbtn.src = '/assets/entrantes_boton.jpg';
      
      const divRecPrincipal = document.createElement('div');
      divRecPrincipal.classList.add('recetas__grupos__principal', 'recetas_tipo');
      const titleRec2 = document.createElement('h3');
      titleRec2.textContent = 'Principales';
      const p2 = document.createElement('p');
      p2.classList.add('recetas__grupos__txt2');
      p2.textContent='Pulsa aqui para ver todas las recetas de platos principales';
      const divimg2 = document.createElement('div');
      divimg2.classList.add('recetas__grupos__divimg');
      const imgbtn2 = document.createElement('img');
      imgbtn2.classList.add('recetas__grupos__img')
      imgbtn2.src = '/assets/principal_boton.jpg';
  
      const btnRecEntre2 = document.createElement('a');
      btnRecEntre2.addEventListener('click', ()=>{
          verPrincipales();
          
      });
      btnRecEntre2.classList.add('recetas__grupos__boton', 'btn_rec2');
  
  
      const divRecPostres = document.createElement('div');
      divRecPostres.classList.add('recetas__grupos__postre', 'recetas_tipo');
      const titleRec3 = document.createElement('h3');
      titleRec3.textContent = 'Postres';
      const p3 = document.createElement('p');
      p3.classList.add('recetas__grupos__txt3');
      p3.textContent='Pulsa aqui para ver todas las recetas de postres';
      const divimg3 = document.createElement('div');
      divimg3.classList.add('recetas__grupos__divimg');
      const imgbtn3 = document.createElement('img');
      imgbtn3.classList.add('recetas__grupos__img')
      imgbtn3.src = '/assets/postres_boton.jpeg';
  
      const btnRecEntre3 = document.createElement('a');
      btnRecEntre3.addEventListener('click', ()=>{
        verPostres();
      });
      btnRecEntre3.classList.add('recetas__grupos__boton');
  
      divPrincipal$$.appendChild(sectionRecetario$$);
      //main.appendChild(sectionRecetario$$);
      sectionRecetario$$.appendChild(divRecetario);
      divRecetario.appendChild(btnRecEntre);
      btnRecEntre.appendChild(divRecEntrante);
      divRecEntrante.appendChild(titleRec);
      divRecEntrante.appendChild(p);
      divRecEntrante.appendChild(divimg);
      divimg.appendChild(imgbtn);
      divRecetario.appendChild(divRecPrincipal);
      divRecetario.appendChild(btnRecEntre2);
      btnRecEntre2.appendChild(divRecPrincipal);
      divRecetario.appendChild(divRecPostres);
      divRecetario.appendChild(btnRecEntre3);
      btnRecEntre3.appendChild(divRecPostres);
      divRecPrincipal.appendChild(titleRec2);
      divRecPrincipal.appendChild(p2);
      divRecPrincipal.appendChild(divimg2);
      divimg2.appendChild(imgbtn2);
      divRecPostres.appendChild(titleRec3);
      divRecPostres.appendChild(p3);
      divRecPostres.appendChild(divimg3);
      divimg3.appendChild(imgbtn3);

//--------FIN section recetario

      const verEntrantes = () => {
        typesRecetas();
        const title = document.querySelector('.titulo_receta');
        title.textContent=('Entrantes');

        for(receta of recetas){

          const contReceta = document.createElement('div');
          contReceta.classList.add('recetaE__card');
          let NombreReceta= document.createElement('h4');
          NombreReceta.textContent=receta.name;
          
          const imgReceta= document.createElement('img');
          imgReceta.classList.add('entrantes__img');
          imgReceta.src=receta.photo;

          const btn = document.createElement('button');
          btn.classList.add('recetas__entrantes-btn');
          btn.textContent=('Ver receta completa');
          
          contReceta.appendChild(NombreReceta);
          contReceta.appendChild(imgReceta);
          contReceta.appendChild(btn);
          let click=0

          const openRece = () =>{
            contReceta.style.height = 'auto';
         
            click++;
            console.log(click)
           
            if(click%2==0){
              contReceta.style.height = '40rem';
              click=0;
            }
          }
          btn.addEventListener('click', openRece);

          for(let i=0; i<receta.ingredients.length;i++){
            const ingredients$$ = document.createElement('p');
            ingredients$$.classList.add('recetas__entrantes__txt')
            ingredients$$.innerHTML =  receta.ingredients[i];
            
              contReceta.appendChild(ingredients$$)
              
          }
          const titlePreparation = document.createElement('p');
          titlePreparation.classList.add('card__preparation');
          titlePreparation.textContent=('Como se prepara?');
          const preparation = document.createElement('p');
          preparation.classList.add('recetas__entrantes__txt')
          preparation.textContent=receta.preparation;
          contReceta.appendChild(titlePreparation)
          contReceta.appendChild(preparation)
          const divEntrantes = document.querySelector('.recetas__entrantes');
          
          if(receta.tipo=='Entrante'){

          divEntrantes.appendChild(contReceta);
        
        }

      
    }
      
    }
    const verPrincipales = () =>{
      typesRecetas();
      const title = document.querySelector('.titulo_receta');
      title.textContent=('Principales');

      for(receta of recetas){

        const contReceta = document.createElement('div');
        contReceta.classList.add('recetaE__card');
        let NombreReceta= document.createElement('h4');
        NombreReceta.textContent=receta.name;
        
        const imgReceta= document.createElement('img');
        imgReceta.classList.add('entrantes__img');
        imgReceta.src=receta.photo;

        const btn = document.createElement('button');
        btn.classList.add('recetas__entrantes-btn');
        btn.textContent=('Ver receta completa');
        
        contReceta.appendChild(NombreReceta);
        contReceta.appendChild(imgReceta);
        contReceta.appendChild(btn);
        let click=0

        const openRece = () =>{
          contReceta.style.height = 'auto';
       
          click++;
          console.log(click)
         
          if(click%2==0){
            contReceta.style.height = '40rem';
            click=0;
          }
        }
        btn.addEventListener('click', openRece);

        for(let i=0; i<receta.ingredients.length;i++){
          const ingredients$$ = document.createElement('p');
          ingredients$$.classList.add('recetas__entrantes__txt')
          ingredients$$.innerHTML =  receta.ingredients[i];
          
            contReceta.appendChild(ingredients$$)
            
        }
        const titlePreparation = document.createElement('p');
        titlePreparation.classList.add('card__preparation');
        titlePreparation.textContent=('Como se prepara?');
        const preparation = document.createElement('p');
        preparation.classList.add('recetas__entrantes__txt')
        preparation.textContent=receta.preparation;
        contReceta.appendChild(titlePreparation)
        contReceta.appendChild(preparation)
     
      if(receta.tipo=='Principal'){
        const divEntrantes = document.querySelector('.recetas__entrantes');
        
        divEntrantes.appendChild(contReceta);
        
      }
    }

    }
    const verPostres=() =>{
      typesRecetas();
      const title = document.querySelector('.titulo_receta');
      title.textContent=('Postres');
     
      for(receta of recetas){

        const contReceta = document.createElement('div');
        contReceta.classList.add('recetaE__card');
        let NombreReceta= document.createElement('h4');
        NombreReceta.textContent=receta.name;
        
        const imgReceta= document.createElement('img');
        imgReceta.classList.add('entrantes__img');
        imgReceta.src=receta.photo;

        const btn = document.createElement('button');
        btn.classList.add('recetas__entrantes-btn');
        btn.textContent=('Ver receta completa');
        
        contReceta.appendChild(NombreReceta);
        contReceta.appendChild(imgReceta);
        contReceta.appendChild(btn);
        let click=0

        const openRece = () =>{
          contReceta.style.height = 'auto';
       
          click++;
          console.log(click)
         
          if(click%2==0){
            contReceta.style.height = '40rem';
            click=0;
          }
        }
        btn.addEventListener('click', openRece);

        for(let i=0; i<receta.ingredients.length;i++){
          const ingredients$$ = document.createElement('p');
          ingredients$$.classList.add('recetas__entrantes__txt')
          ingredients$$.innerHTML =  receta.ingredients[i];
          
            contReceta.appendChild(ingredients$$)
            
        }
        const titlePreparation = document.createElement('p');
        titlePreparation.classList.add('card__preparation');
        titlePreparation.textContent=('Como se prepara?');
        const preparation = document.createElement('p');
        preparation.classList.add('recetas__entrantes__txt')
        preparation.textContent=receta.preparation;
        contReceta.appendChild(titlePreparation)
        contReceta.appendChild(preparation)
        const divEntrantes = document.querySelector('.recetas__entrantes');
        if(receta.tipo=='Postre'){
          const divEntrantes = document.querySelector('.recetas__entrantes');
          
          divEntrantes.appendChild(contReceta);
          
        }
      }
    
    }
    const typesRecetas = ()=>{
      divPrincipal$$.innerHTML='';
        header();
        const divEntrantes = document.createElement('div');
        divEntrantes.classList.add('recetas__entrantes', 'container');
        const title = document.createElement('h2');
        title.classList.add('titulo_receta');
        const aImg = document.createElement('img');
        aImg.src='/assets/cerrar.png'
        aImg.addEventListener('click', ()=>{
          divPrincipal$$.innerHTML='';
          Inicio();
        })

        divPrincipal$$.appendChild(divEntrantes);
        divEntrantes.appendChild(aImg)
        divEntrantes.appendChild(title);
         
    }
  }

  )};